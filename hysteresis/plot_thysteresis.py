import numpy as np
from matplotlib import pyplot as plt
from mumaxplot.io.parser import Parser
from mumaxplot.io.table import loadtable

plt.rcParams['figure.figsize'] = 10, 10
plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'


def thysteresis():
    parser = Parser()
    m_index = parser.m_index()
    B_index = parser.B_index()

    with open('table.txt', 'r') as f:
        lines = f.readlines()

    # t, mx, my, mz, B_extx, B_exty, B_extz = [], [], [], [], [], [], []

    t, mx, B_extx = loadtable('table.txt', usecols=(0, m_index, B_index))

    t = np.array(t).astype(float) * 1e9
    B_extx = np.array(B_extx).astype(float)
    mx = np.array(mx).astype(float)


    fig, ax1 = plt.subplots()
    ax1.plot(t, mx, 'b-', linewidth=2, markeredgecolor='k')
    ax1.set_xlabel('time (ns)')

    ax1.axhline(0.0, linestyle='--', linewidth=1)
    ax1.axvline(0.0, linestyle='--', linewidth=1)

    # ax2 = ax1.twinx()
    # ax2.plot(t, B_extx, 'k-', linewidth=1)
    # ax2.set_ylim((-0.5, 0.5))
    # ax2.set_ylabel('B$_x$ (T)')
    # ax2.set_yticks((-0.1, 0., 0.1))

    # ax1.set_ylim((-1.05, 1.05))

    ax1.set_xlabel("time (ns)")
    m_label = parser.m_label()
    ax1.set_ylabel(f"m$_{m_label}$")

    if parser.savepng:
        fname = (parser.pngname
                 if parser.pngname is not None
                 else "thysteresis.png")
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    plt.show()

    
if __name__ == '__main__':
    thysteresis()
