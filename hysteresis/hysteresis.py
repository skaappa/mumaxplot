import numpy as np
from matplotlib import pyplot as plt
from mumaxplot.io.parser import Parser
from mumaxplot.folders import get_folders
from mumaxplot.io.table import loadtable

plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'

def single_hysteresis():
    parser = Parser()
    m_index = parser.m_index()
    B_index = parser.B_index()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    mx, Bx = loadtable('table.txt', usecols=(m_index, B_index))

    if mx is None:
        return

    Bx *= 1e3  # milliteslas

    plt.axhline(0.0, linestyle='--', linewidth=1)
    plt.axvline(0.0, linestyle='--', linewidth=1)

    if parser.twocolor:
        lim = int(len(Bx) / 2)
        plt.plot(Bx[:lim+1], mx[:lim+1], 'bo-', linewidth=4, markeredgecolor='k')
        plt.plot(Bx[lim:], mx[lim:], 'ro-', linewidth=4, markeredgecolor='k')
    else:
        plt.plot(Bx, mx, 'b-', linewidth=2, markeredgecolor='k')

    finish_plot(parser)


def multihysteresis():
    parser = Parser()
    m_index = parser.m_index()
    B_index = parser.B_index()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    bigfolder = '.'

    folders = get_folders(bigfolder)

    for findex, folder in enumerate(folders):
        mx, Bx = loadtable('{:s}/calc.out/table.txt'
                           .format(folder),
                           usecols=(m_index, B_index))

        if mx is None:
            continue

        Bx *= 1e3  # milliteslas

        plt.plot(Bx, mx,
                 linestyle='-', linewidth=1,
                 marker=None)

        if parser.printfolders:
            print(folder)

    finish_plot(parser)


def finish_plot(parser):

    m_label = parser.m_label()

    plt.axhline(0.0, linestyle='--', linewidth=1, zorder=-1)
    plt.axvline(0.0, linestyle='--', linewidth=1, zorder=-1)

    if parser.xlim is None:
        plt.xlim((-105.0, 105.0))
    else:
        plt.xlim(parser.xlim)

    if parser.ylim is None:
        plt.ylim((-1.05, 1.05))
    else:
        plt.ylim(parser.ylim)

    plt.xlabel("B$_\mathrm{ext}$ (mT)")  # TODO: Add B_label?
    plt.ylabel("m$_{:s}$".format(m_label))

    if parser.savepng:
        fname = (parser.pngname
                 if parser.pngname is not None
                 else "hysteresis.png")
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    if parser.showfig:
        plt.show()
