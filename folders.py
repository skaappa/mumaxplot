import os
import numpy as np

def get_folders(bigfolder, sort=True):
    folders = []
    
    # bigfolder might be of type numpy.str. Lets convert it:
    bigfolder = str(bigfolder)
    
    for filename in os.listdir(bigfolder):
        d = os.path.join(bigfolder, filename)
        if os.path.isdir(d):
            folders.append('{:s}/{:s}'.format(bigfolder, filename))

    if sort:
        return np.sort(folders)
        
    return folders
