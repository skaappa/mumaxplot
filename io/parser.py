from optparse import OptionParser
import numpy as np


class Parser:

    def __init__(self):
        parser0 = OptionParser()
        parser0.add_option("-t", "--plottype", type='string')
        parser0.add_option("-s", "--save", action="store_true", dest="savepng")
        parser0.add_option("-n", "--dont-show",
                           action="store_false", dest="showfig")
        parser0.add_option("--twocolor",
                           action="store_true")
        parser0.add_option('--xlim',
                           type='string',
                           action='callback',
                           callback=self.parse2list)
        parser0.add_option('--ylim',
                           type='string',
                           action='callback',
                           callback=self.parse2list)
        parser0.add_option('--figsize',
                           type='string',
                           action='callback',
                           callback=self.parse2list)
        parser0.add_option('--pngname', type='string')
        parser0.add_option('--filename', type='string')
        parser0.add_option("-x", action="store_true", dest="x")
        parser0.add_option("-y", action="store_true", dest="y")
        parser0.add_option("-z", action="store_true", dest="z")
        parser0.add_option('--Bdir', type='string')
        parser0.add_option("--printfolders", action="store_true")
        parser0.add_option("--binwidth", type=float)
        parser0.add_option("--dmx_llim", type=float)
        parser0.add_option("--dont-fit", action="store_false", dest="fit")
        parser0.add_option("--fit_llim", type=float)
        parser0.add_option("--fit_ulim", type=float)
        parser0.add_option("--bigfolders",
                           type='string',
                           action='callback',
                           callback=self.parse2list)
        parser0.add_option("--folders",
                           type='string',
                           action='callback',
                           callback=self.parse2list)

        parser0.add_option("--labels",
                           type='string',
                           action='callback',
                           callback=self.parse2list)
        parser0.add_option("--finishlimit", type=float)
        parser0.add_option("--threshold", type=float)
        parser0.add_option("--thresholds",
                           type='string',
                           action='callback',
                           callback=self.parse2list)
        parser0.add_option("--noreread", action="store_false", dest="reread")
        parser0.add_option("--log", action="store_true")
        parser0.add_option("--kmeans_k", type=int)
        parser0.add_option("--positive",
                           type=int,
                           action='callback',
                           callback=self.parse2bool)
        parser0.add_option("--median_window", type=float)  # nanoseconds
        parser0.add_option("--layer", type=int)
        
        self.options, self.args = parser0.parse_args()

    def parse2bool(self, option, opt, value, parser):
        setattr(parser.values, option.dest, bool(value))

    @property
    def plottype(self):
        return self.options.plottype

    @property
    def savepng(self):
        return self.options.savepng if self.options.savepng is not None else False

    @property
    def showfig(self):
        return self.options.showfig if self.options.showfig is not None else True

    @property
    def twocolor(self):
        return self.options.twocolor if self.options.twocolor is not None else False

    @property
    def xlim(self):
        return np.array(self.options.xlim, dtype=float) if self.options.xlim is not None else None

    @property
    def ylim(self):
        return np.array(self.options.ylim, dtype=float) if self.options.ylim is not None else None

    @property
    def figsize(self):
        return np.array(self.options.figsize, dtype=float) if self.options.figsize is not None else None

    @property
    def pngname(self):
        return self.options.pngname

    @property
    def filename(self):
        return self.options.filename

    @property
    def y(self):
        return self.options.y if self.options.y is not None else False

    @property
    def z(self):
        return self.options.z if self.options.z is not None else False

    @property
    def printfolders(self):
        return (self.options.printfolders
                if self.options.printfolders is not None
                else False)

    @property
    def binwidth(self):
        return self.options.binwidth

    @property
    def dmx_llim(self):
        return self.options.dmx_llim

    @property
    def fit_llim(self):
        return self.options.fit_llim

    @property
    def fit_ulim(self):
        return self.options.fit_ulim

    @property
    def fit(self):
        return (self.options.fit
                if self.options.fit is not None
                else True)

    @property
    def bigfolders(self):
        return (np.array(self.options.bigfolders, dtype=str)
                if self.options.bigfolders is not None
                else ['.'])

    @property
    def folders(self):
        return (np.array(self.options.folders, dtype=str)
                if self.options.folders is not None
                else ['.'])

    @property
    def labels(self):
        return ([str(label) for label in self.options.labels]
                if self.options.labels is not None
                else self.bigfolders)

    @property
    def finishlimit(self):
        return (self.options.finishlimit
                if self.options.finishlimit is not None
                else 0.0)

    @property
    def threshold(self):
        return self.options.threshold

    @property
    def thresholds(self):
        return [float(t) for t in self.options.thresholds]

    @property
    def reread(self):
        val = self.options.reread
        return val if val is not None else True

    @property
    def log(self):
        val = self.options.log
        return val if val is not None else False

    @property
    def kmeans_k(self):
        return (self.options.kmeans_k
                if self.options.kmeans_k is not None
                else 1)

    @property
    def positive(self):
        return self.options.positive

    def parse2list(self, option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    @property
    def median_window(self):
        value = self.options.median_window
        default = 10.0  # ns
        return value if value is not None else default

    def m_index(self):
        if self.options.x:
            return 1
        if self.options.y:
            return 2
        if self.options.z:
            return 3

        # otherwise x:
        return 1

    def m_label(self):
        if self.options.x:
            return 'x'
        if self.options.y:
            return 'y'
        if self.options.z:
            return 'z'

        # otherwise x:
        return 'x'


    def B_index(self):
        '''
        Return table.txt column index that includes the B field, given by Bdir
        '''
        indices = {'x': 4, 'y': 5, 'z': 6}
        
        if self.options.Bdir is None:
            return indices['x']

        return indices[self.options.Bdir]

    def B_label(self):
        return self.options.Bdir

    @property
    def layer(self):
        return self.options.layer if self.options.layer is not None else 0
