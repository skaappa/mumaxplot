import numpy as np


def loadtable(fname, usecols):
    try:
        with open(fname, 'r') as f:
            lines = f.readlines()
    except FileNotFoundError:
        print('File not found: {:s}'.format(fname))
        return [None] * len(usecols)

    data = []

    for line in lines:
        if line.startswith('#'):
            continue

        row = line.split()

        data.append([row[usecols[i]] for i in range(len(usecols))])

    data = np.array(data, dtype=float).T
    return data
