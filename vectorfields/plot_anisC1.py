import numpy as np
from matplotlib import pyplot as plt

plt.rcParams['figure.figsize'] = 8, 8

data = np.load('anisC1.npy').astype(float)

X, Y = range(data.shape[3]), range(data.shape[2])
U, V = data[0, 0, :, :], data[1, 0, :, :]

xdel, ydel = 8, 8

X, Y, U, V = X[::xdel], Y[::ydel], U[::ydel, ::xdel], V[::ydel, ::xdel]

cont = plt.contourf(np.sqrt(data[0, 0, :, :]**2 + data[1, 0, :, :]**2),
                    levels=200, cmap='gist_ncar')

plt.colorbar(cont)

plt.quiver(X, Y, U, V, alpha=0.5)

plt.gca().set_aspect('equal')
plt.show()

