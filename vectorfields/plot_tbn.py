import numpy as np
from matplotlib import pyplot as plt

plt.rcParams['figure.figsize'] = 10, 10
plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'

with open('table.txt', 'r') as f:
    lines = f.readlines()

t, mx, my, mz, B_extx, B_exty, B_extz = [], [], [], [], [], [], []

for line in lines:
    if line.startswith('#'):
        continue
    
    row = line.split()
    
    t.append(row[0])
    B_extx.append(row[4])
    mx.append(row[1])
    
t = np.array(t).astype(float)
B_extx = np.array(B_extx).astype(float)
dt = t[1] - t[0]

mx = np.array(mx).astype(float)
# dmx = np.gradient(mx) / dt

dmx = []
for i in range(1, len(mx)):
    dmx.append((mx[i] - mx[i-1]) / (B_extx[i] - B_extx[i-1]))

dmx = np.array(dmx)

t = 1e9 * t[1:]
plt.plot(t, dmx, 'b-', linewidth=1, markeredgecolor='k')

plt.xlabel("t (ns)")
plt.ylabel("dm$_x$/dB$_\mathrm{ext}$ (A/Tm)")
plt.show()


