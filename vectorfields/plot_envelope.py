import numpy as np
from matplotlib import pyplot as plt

plt.rcParams['figure.figsize'] = 10, 10
plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'

with open('table.txt', 'r') as f:
    lines = f.readlines()

t, mx, my, mz, B_extx, B_exty, B_extz = [], [], [], [], [], [], []

for line in lines:
    if line.startswith('#'):
        continue
    
    row = line.split()
    
    B_extx.append(row[4])
    mx.append(row[1])


B_extx = np.array(B_extx).astype(float)
mx = np.array(mx).astype(float)

plt.plot(B_extx[0:200], mx[0:200] - mx[200:400][::-1], 'b-', linewidth=4, markeredgecolor='k')

plt.xlabel("B$_\mathrm{ext}$ (T)")
plt.show()


