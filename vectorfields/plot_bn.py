import numpy as np
from matplotlib import pyplot as plt
import warnings

plt.rcParams['figure.figsize'] = 10, 10
plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'

with open('table.txt', 'r') as f:
    lines = f.readlines()

t, mx, my, mz, B_extx, B_exty, B_extz = [], [], [], [], [], [], []

for line in lines:
    if line.startswith('#'):
        continue
    
    row = line.split()
    
    B_extx.append(row[4])
    mx.append(row[1])

B_extx = np.array(B_extx, dtype=float)
mx = np.array(mx, dtype=float)

Bsign = np.sign(np.diff(B_extx))

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    dmx = np.diff(mx) / Bsign

dmx = dmx[Bsign < 0]

finite = np.isfinite(dmx)
B_extx = B_extx[:-1][Bsign < 0][finite]
dmx = dmx[finite]


# lim = int(len(B_extx) / 2)
plt.plot(B_extx, dmx, 'bo-', linewidth=2, markeredgecolor='k')
# plt.plot(B_extx[lim:], dmx[lim:], 'ro-', linewidth=2, markeredgecolor='k')

plt.xlabel("B$_\mathrm{ext}$ (T)")
plt.ylabel("$\Delta$m$_x$")
plt.show()


