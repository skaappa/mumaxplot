import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import linregress
from scipy.optimize import curve_fit
from header import get_headerdata
from optparse import OptionParser
import os, warnings

plt.rcParams['figure.figsize'] = 10, 10
plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'

parser = OptionParser()
parser.add_option("-s", "--save", action="store_true", dest="savepng")
(options, args) = parser.parse_args()

savepng = options.savepng if options.savepng is not None else False

bigfolders = ['.']
labels = [bigfolders[0]]

binwidth = 1 / 6.

def P(s, tau, s0, prefactor):
    return prefactor * s**(-tau) * np.exp(-s / s0)


def logP(s, tau, s0, prefactor, printerror=False):
    if printerror:
        fit_residual_error = np.sqrt(np.mean(np.square(logP(s, tau, s0,
                                                            prefactor,
                                                            printerror=False)
                                                       - y)))
        print(tau, s0, prefactor, fit_residual_error)
    return np.log10(prefactor) - tau * s - 10**s / (s0 * np.log(10))


bigfolders_peaks = []

# Read data and convert to peaks:
for bfindex, bigfolder in enumerate(bigfolders):

    bigfolders_peaks.append([])

    folders = ['.']
    # for filename in os.listdir(bigfolder):
    #     d = os.path.join(bigfolder, filename)
    #     if os.path.isdir(d):
    #         folders.append('{:s}/{:s}'.format(bigfolder, filename))

    for folder in folders:
        # Get data:
        try:
            with open('{:s}/table.txt'.format(folder), 'r') as f:
                lines = f.readlines()
        except:
            print('File not found: '
                  '{:s}/table.txt'.format(folder))
            continue

        t, mx, my, mz, B_extx, B_exty, B_extz = [], [], [], [], [], [], []

        for line in lines:
            if line.startswith('#'):
                continue

            row = line.split()

            B_extx.append(row[4])
            mx.append(row[1])

        B_extx = np.array(B_extx).astype(float)
        mx = np.array(mx).astype(float)

        # Resolve gradient:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            dmx = np.diff(mx) / np.sign(np.diff(B_extx))

        dmx = dmx[np.isfinite(dmx)]

        # Resolve peaks:
        peaks = dmx[np.argwhere(dmx > 0.)]

        bigfolders_peaks[bfindex] += list(peaks)

# Get minimum and maximum bin locations:
peak_min, peak_max = np.inf, 0.
for peaks in bigfolders_peaks:
    if np.min(peaks) < peak_min:
        peak_min = np.min(peaks)
    if np.max(peaks) > peak_max:
        peak_max = np.max(peaks)

nbins = int((np.log10(peak_max) + 0.1 - np.log10(peak_min)) / binwidth)
bins = np.logspace(np.log10(peak_min), np.log10(peak_max)+0.1, nbins)

# bins = np.logspace(np.log10(peak_min), np.log10(peak_max), nbins)

# Get bin centers for fit and plotting:
bincenters = []
for i in range(nbins - 1):
    bincenters.append((bins[i+1] + bins[i]) / 2)
bincenters = np.array(bincenters)
    
# Get region to fit:
linreg_llim, linreg_ulim = 3e-3, 2
linreg_llim_idx = (np.abs(bincenters - linreg_llim)).argmin()
linreg_ulim_idx = (np.abs(bincenters - linreg_ulim)).argmin() + 1

for bfindex, bigfolder_peaks in enumerate(bigfolders_peaks):

    # Calculate histogram:
    hist, bin_edges = np.histogram(bigfolder_peaks, bins=bins)
    hist = hist.astype(float)

    # Weight by bin width:
    hist /= np.diff(bin_edges)

    # Normalize:
    hist /= np.sum(hist)

    # Scale for readability
    scalefactor = 1

    # Fit curve:
    ydata_to_fit = hist[linreg_llim_idx:]
    nonzero = np.argwhere(ydata_to_fit > 0)
    x = np.log10(bincenters)[linreg_llim_idx:][nonzero].flatten()
    y = np.log10(ydata_to_fit[nonzero]).flatten()


    # Remove infinities and nans:
    # non_inf = np.isfinite(y)
    # x = x[non_inf]
    # y = y[non_inf]

    # do fitting:
    # trick with prefactor:
    y_to_fit = 12 + y
    (tau, s0, prefactor), pcov = curve_fit(logP, x, y_to_fit,
                                           p0 = [1.5, 4e-2, 1e-12],
                                           ftol=1e-12,
                                           xtol=1e-12,
                                           # gtol=1e-12,
                                           loss='linear',
                                           max_nfev=10000,
                                           bounds=np.array(([0., np.inf],
                                                            [0., np.inf],
                                                            [-1, np.inf])).T)

    prefactor *= 1e-12  # end of trick
    

    # plot fitted curve:
    xx = np.linspace(min(x), max(x), 40)
    plt.plot(10**xx,
             np.power(10, logP(xx, tau, s0, prefactor, printerror=False)) * scalefactor,
             'k--',
              zorder=-1)

    # Calculate error of the fit:
    fit_residual_error = np.sqrt(np.mean(np.square(logP(x, tau, s0, prefactor, printerror=False) - y)))

    # Print curve fit:
    print("tau: {:04f}    s0: {:.02E}    prefactor: {:.04f}    error: {:.04f}"
          .format(tau, s0, prefactor, fit_residual_error))

    # Plot histogram:
    plt.loglog(bincenters,
               hist * scalefactor, linestyle='none', marker='o',
               linewidth=4, markersize=8,
               label=labels[bfindex] + r" ($\tau$={:.02f}, $s_0$={:.02E})".format(tau, s0))

    # Plot error bars:
    # plt.errorbar(bincenters, histmean * scalefactor, yerr=histstd * scalefactor,
    #              linestyle='none', capsize=2, color='black')

plt.xlim((1e-4, plt.xlim()[1]))
# plt.xlim((peak_min, plt.xlim()[1]))
# plt.ylim((1e-25, scalefactor))
# plt.ylim((plt.ylim()[0], scalefactor))

# Fill area for fitted data region:
import matplotlib.transforms as mtransforms
trans = mtransforms.blended_transform_factory(plt.gca().transData,
                                              plt.gca().transAxes)
plt.gca().fill_between(bincenters[linreg_llim_idx:], 0, 1,
                       facecolor='green', alpha=0.2,
                       zorder=-1, transform=trans,
                       label="Data region for curve fit")

# Add text about the fit function:
plt.text(10**((x[-1] + x[0]) / 2),
         0.05,
         r"Fit function: $P(s) = k_0s^{-{\tau}}e^{-s/s_0}$",
         transform=trans,
         verticalalignment='bottom',
         horizontalalignment='center')

# Plot options:
plt.legend(title='Disorder strength')
plt.xlabel("Peak height $\Delta$m$_x$")
plt.ylabel("Probability")

if savepng:
    fname = "bndist.png"
    plt.savefig(fname, dpi=200, bbox_inches='tight')

plt.show()
