import numpy as np
from matplotlib import pyplot as plt
from header import get_headerdata

plt.rcParams['figure.figsize'] = 10, 10
plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'

llim = 1e-1
ulim = 1e3
nbins = 60
window = llim
bins = np.logspace(np.log10(llim), np.log10(ulim), nbins)
hist = np.zeros(len(bins) - 1)

with open('table.txt', 'r') as f:
    lines = f.readlines()

t, mx, my, mz, B_extx, B_exty, B_extz = [], [], [], [], [], [], []

for line in lines:
    if line.startswith('#'):
        continue
    
    row = line.split()

    t.append(row[0])
    B_extx.append(row[4])
    mx.append(row[1])
    
B_extx = np.array(B_extx).astype(float)
t = np.array(t).astype(float)
mx = np.array(mx).astype(float)

dmx = []
for i in range(1, len(mx)):
    dmx.append((mx[i] - mx[i-1]) / (B_extx[i] - B_extx[i-1]))
dmx = np.array(dmx)

peaks = dmx

hist = np.histogram(peaks, bins=bins)[0] * bins[:-1]**-1

plt.loglog(bins[:-1], hist, 'bo-')

# Linear regression:
from scipy.stats import linregress
x = np.log(bins[:10])
y = np.log(hist[:10])
slope, intercept, r, p, se = linregress(x, y)
plt.plot(np.exp(x), np.exp(intercept + slope * x), 'k--', label='Slope: {:.02f}'.format(slope), zorder=-1)
# plt.text(x=llim, y=np.min(hist), s='Slope: {:.02f}'.format(slope))

plt.legend()
plt.xlabel("Peak height dm$_x$/dB$_\mathrm{ext}$ (A/Tm)")
plt.ylabel("Count")
plt.show()

