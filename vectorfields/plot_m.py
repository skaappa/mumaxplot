import numpy as np
from matplotlib import pyplot as plt
from header import get_headerdata

plt.rcParams['figure.figsize'] = 8, 8
plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'

data = np.load('m.npy').astype(float)

header = get_headerdata()
hx = header['xstepsize']
hy = header['ystepsize']
gx = header['xnodes']
gy = header['xnodes']

x = np.arange(0, hx * gx, hx) * 1e9
y = np.arange(0, hy * gy, hy) * 1e9

U, V = data[0, 0, :, :], data[1, 0, :, :]

xdel, ydel = 8, 8

X, Y, U, V = x[::xdel], y[::ydel], U[::ydel, ::xdel], V[::ydel, ::xdel]

# cont = plt.contourf(x, y, np.sqrt(data[0, 0, :, :]**2 + data[1, 0, :, :]**2),
#                     levels=100, cmap='autumn')

contourdata = np.angle(data[0, 0, :, :] + data[1, 0, :, :] * 1j)
cont = plt.contourf(x, y, contourdata,
                    levels=[l * np.pi for l in np.arange(-1., 1., 0.02)],
                    cmap='hsv', vmin=-np.pi, vmax=np.pi)

# from matplotlib.cm import ScalarMappable
# plt.colorbar(ScalarMappable(cmap='hsv'))    

# plt.colorbar(cont)

plt.quiver(X, Y, U, V, alpha=0.5)

plt.gca().set_aspect('equal')

plt.xlabel('x (nm)')
plt.ylabel('y (nm)')

plt.show()

