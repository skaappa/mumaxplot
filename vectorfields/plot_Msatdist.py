import numpy as np
from matplotlib import pyplot as plt
from header import get_headerdata
from optparse import OptionParser

plt.rcParams['figure.figsize'] = 14, 6
plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'

parser = OptionParser()
parser.add_option("-s", "--save", action="store_true", dest="savepng")
(options, args) = parser.parse_args()

savepng = options.savepng if options.savepng is not None else False


data = np.load('Msat.npy').astype(float)

header = get_headerdata()
hx = header['xstepsize']
hy = header['ystepsize']
gx = header['xnodes']
gy = header['xnodes']

x = np.arange(0, hx * gx, hx) * 1e9
y = np.arange(0, hy * gy, hy) * 1e9

fig, (ax1, ax2) = plt.subplots(1, 2)

fig.tight_layout()

cont = ax1.contourf(x, y, data[0, 0, :, :], levels=200, cmap='autumn')
# plt.colorbar(cont)
ax1.set_aspect('equal')
ax1.set_xlabel('x (nm)')
ax1.set_ylabel('y (nm)')


ax2.hist(data[0, 0, :, :].flatten(), bins=20)
ax2.set_xlabel("M$_\mathrm{sat}$ (A/m)")
ax2.set_ylabel("Cell count")

if savepng:
    fname = "Msatdist.png"
    # print("Saving {:s}".format(fname))
    plt.savefig(fname, dpi=200, bbox_inches='tight')


plt.show()
