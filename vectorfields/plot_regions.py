import numpy as np
from matplotlib import pyplot as plt
from header import get_headerdata

plt.rcParams['figure.figsize'] = 8, 8
plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'

data = np.load('regions.npy').astype(float)

header = get_headerdata()
hx = header['xstepsize']
hy = header['ystepsize']
gx = header['xnodes']
gy = header['xnodes']

x = np.arange(0, hx * gx, hx) * 1e9
y = np.arange(0, hy * gy, hy) * 1e9

cont = plt.contour(x, y, data[0, 0, :, :], levels=256)
plt.colorbar(cont)

plt.gca().set_aspect('equal')

plt.xlabel('x (nm)')
plt.ylabel('y (nm)')

plt.show()

