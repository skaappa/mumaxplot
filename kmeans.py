import numpy as np


def kmean(signal, k=10, median=False):
    halfk = int(k / 2)
    new_signal = np.zeros_like(signal)

    method = np.median if median else np.mean

    for i in range(halfk, len(signal) - halfk):
        new_signal[i] = method(signal[i - halfk : i + halfk])
    return new_signal


def krms(signal, k=10):
    halfk = int(k / 2)

    signal = signal.reshape(-1)

    new_signal = np.zeros_like(signal)
    for i in range(len(signal)):
        start = max(0, i - halfk)
        end = min(len(signal), i + halfk + 1)
        new_signal[i] = np.sqrt(np.mean(np.square(signal[start : end])))

    return new_signal

