import numpy as np


def strip_too_small_elements(array, limit):
    return np.array(array)[array > limit]


def get_nbins(vmin, vmax, binwidth):
    return int((np.log10(vmax) + 0.1 - np.log10(vmin)) / binwidth)


def get_bins(vmin, vmax, nbins):
    return np.logspace(np.log10(vmin), np.log10(vmax) + 0.1, nbins)


def get_bincenters(bins):
    '''
    bins are given as left-bound values.
    '''
    nbins = len(bins)
    bincenters = []
    for i in range(nbins - 1):
        bincenters.append((bins[i + 1] + bins[i]) / 2)
    bincenters = np.array(bincenters)
    return bincenters


def get_histogram(data, bins, normalize=True):
    hist, bin_edges = np.histogram(data, bins=bins)

    hist = hist.astype(float)

    # Weight by bin width:
    hist /= np.diff(bin_edges)

    if normalize:
        hist /= np.sum(hist)

    return hist
