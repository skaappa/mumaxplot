import numpy as np
from matplotlib import pyplot as plt

def regions():
    data = np.load('regions.npy')
    print(data.shape)

    n = len(set(data[0, 0, :, :].ravel()))

    cont = plt.contourf(data[0, 0, :, :], levels=n, cmap='flag')
    # plt.colorbar(cont)

    plt.gca().set_aspect('equal')
    plt.show()


def Msat():
    data = np.load('Msat.npy')
    print(data.shape)

    n = len(set(data[0, 0, :, :].ravel()))

    cont = plt.pcolormesh(data[0, 0, :, :], cmap='plasma')
    # plt.colorbar(cont)

    plt.gca().set_aspect('equal')
    plt.show()
