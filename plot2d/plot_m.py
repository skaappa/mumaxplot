import numpy as np
from matplotlib import pyplot as plt
from mumaxplot.plot2d.plot import plot_scalarfield, plot_quiver
from mumaxplot.io.parser import Parser
from dislocs.parse import read_gridwidths


def initm():

   parser = Parser()

   fname = parser.filename
   if fname is None:
      fname = 'm.npy'

   data = np.load(fname)
   hx, hy, hz = read_gridwidths()

   # data = np.tile(data, (3, 3))

   scalardata = np.angle(data[1, parser.layer, :, :] + data[0, parser.layer, :, :] * 1j)

   plot_scalarfield(scalardata, hx=hx, hy=hy, label="m",
                    vmin=-np.pi, vmax=np.pi)

   plot_quiver(data, hx=hx, hy=hy, intrvl=24*4)
   plt.gca().set_aspect('equal')

   if parser.savepng:
      fname = (parser.pngname
               if parser.pngname is not None
               else "m.png")
      plt.savefig(fname, dpi=200, bbox_inches='tight')

   if parser.showfig:
      plt.show()
