import numpy as np
from matplotlib import pyplot as plt
from subprocess import Popen

plt.rcParams['figure.figsize'] = 5, 5

Bs = np.loadtxt('table.txt', usecols=4).astype(float) * 1e3
limit = 0.005  # mT
# startindex = np.argwhere(np.abs(np.diff(Bs)) < limit)[0, 0]
startindex = np.argwhere(Bs < 0.)[0, 0]
start = Bs[startindex]

Bstep = 4e-3  # mT
interval = 20

Bs = np.linspace(start, -100, int((start - -100) / (Bstep * interval)))

for layerindex in [0]:

    for i in range(10000):

        # if (i < 300 or i > 500) and i % 20 != 0:
        #     continue
        try:
            data = np.load('m{:06d}.npy'.format(i)).astype(float)
        except FileNotFoundError:
            continue

        X, Y = range(data.shape[3]), range(data.shape[2])
        U, V = data[0, layerindex, :, :], data[1, layerindex, :, :]

        xdel, ydel = 16, 16

        X, Y, U, V = X[::xdel], Y[::ydel], U[::ydel, ::xdel], V[::ydel, ::xdel]

        # contourdata = np.angle(data[1, 0, :, :] + data[0, 0, :, :] * 1j)

        # cont = plt.contourf(contourdata,
        #                     levels=[l * np.pi for l in np.arange(0.01, 1.0, 0.02)],
        #                     cmap='hsv', vmin=0, vmax=np.pi)

        # cont = plt.contourf(contourdata,
        #                     levels=[l * np.pi for l in np.arange(-1.0, 0.01, 0.02)],
        #                     cmap='hsv', vmin=-np.pi, vmax=0)


        m_index = 0
        contourdata = data[m_index, layerindex, :, :]
        cont = plt.contourf(contourdata,
                            levels=np.linspace(-1, 1, 50),
                            cmap='bwr')

        plt.gca().set_facecolor('red')
        # from matplotlib.cm import ScalarMappable
        # plt.colorbar(ScalarMappable(cmap='gist_ncar')) 

        plt.quiver(X, Y, U, V, alpha=0.5,
                   scale=22, scale_units='width',
                   headlength=2, headaxislength=2,
                   width=.01,
                   pivot='mid')

        plt.gca().set_aspect('equal')

        plt.axis('off')

        B = Bs[i]
        props = dict(boxstyle='square', facecolor='white', alpha=0.9)
        plt.text(x=0.06, y=1.01,
                 s='{:.02f} mT'.format(B),
                 fontsize=14,
                 bbox=props,
                 transform=plt.gca().transAxes)

        plt.savefig('png_{:s}_layer{:d}_m{:06d}.png'.format('x',
                                                            layerindex,
                                                            i),
                    dpi=100, bbox_inches='tight')
        plt.clf()
        print(i)

        if i == 0:
            Popen(['xdg-open', 'png_x_layer0_m000000.png'])


