import numpy as np
from matplotlib import pyplot as plt
from dislocs.projectscripts.plot_ms import mycolormap


def get_mesh(nx, ny, hx=1, hy=1):
    x = np.arange(nx)
    y = np.arange(ny)
    xv, yv = np.meshgrid(x, y, indexing='ij')
    xv = np.array(xv) * hx
    yv = np.array(yv) * hy
    return xv, yv


def plot_scalarfield(data, hx=1, hy=1, label="", **kwargs):

    cm = mycolormap()

    xv, yv = get_mesh(nx=data.shape[1],
                      ny=data.shape[0],
                      hx=hx, hy=hy)

    opts = dict(cmap=cm)
    opts.update(**kwargs)
    
    cont = plt.pcolormesh(xv, yv, data.T,
                          **opts)

    cb = plt.colorbar(cont, label=label)

    
def plot_quiver(data, hx=1, hy=1, label="", intrvl=8, **kwargs):

    xv, yv = get_mesh(nx=len(data[0]),
                      ny=len(data[1]),
                      hx=hx, hy=hy)

    
    U, V = data[0, 0, ::intrvl, ::intrvl], data[1, 0, ::intrvl, ::intrvl]

    # Data is ordered as [component, z-coordinate, y-coordinate, x-coordinate].
    # To show it in familiar way, we want the first axis to be x-coordinate
    # and the second one to be y-coordinate. Therefore, let's take transposes:
    U = U.T
    V = V.T

    xv, yv = get_mesh(U.shape[0], U.shape[1], hx=hx, hy=hy)
    xv *= intrvl
    yv *= intrvl

    options = dict(alpha=0.5,
                   scale=22, scale_units='width',
                   headlength=2, headaxislength=2,
                   width=.01,
                   pivot='mid')

    options.update(**kwargs)
    
    plt.quiver(xv, yv, U, V, **options)
