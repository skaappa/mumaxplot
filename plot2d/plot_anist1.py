import numpy as np
from matplotlib import pyplot as plt

def anist1():
    data = np.load('mykt1.npy')[0, 0]
    data *= 1e-3

    n = len(data[0])
    x = np.arange(n)
    y = np.arange(n)
    xv, yv = np.meshgrid(x, y, indexing='ij')
    cont = plt.contourf(xv, yv, data, levels=np.linspace(-30, 30, 100))

    periodic = False
    if periodic:
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                if i == 0 and j == 0:
                    continue
                plt.contourf(xv+i*n, yv+j*n, data, levels=np.linspace(-30, 30, 100))
    cb = plt.colorbar(cont, label="Kt1 (kJ/m3)")

    data = np.load('myanist1.npy')

    intrvl = 8
    U, V = data[0, 0, ::intrvl, ::intrvl], data[1, 0, ::intrvl, ::intrvl]

    print(U.shape)

    # Data is ordered as [component, z-coordinate, y-coordinate, x-coordinate].
    # To show it in familiar way, we want the first axis to be x-coordinate
    # and the second one to be y-coordinate. Therefore, let's take transposes:
    U = U.T
    V = V.T

    xv, yv = np.meshgrid(range(U.shape[0]), range(U.shape[1]), indexing='ij')
    xv *= intrvl
    yv *= intrvl

    print(xv.shape)

    if not periodic:
        plt.quiver(xv, yv, U, V,
                   headlength=0,
                   # headwidth=0,
                   headaxislength=0,
                   pivot='middle')

    plt.gca().set_aspect('equal')
    plt.show()
