import numpy as np
from matplotlib import pyplot as plt
from mumaxplot.plot2d.plot import plot_scalarfield, plot_quiver


def anisc1():
    data = np.load('kc1.npy')[0, 0]
    data *= 1e-3

    plot_scalarfield(data, label="Kc1 (kJ/m3)")
    # n = len(data[0])
    # x = np.arange(n)
    # y = np.arange(n)
    # xv, yv = np.meshgrid(x, y, indexing='ij')
    # cont = plt.contourf(xv, yv, data, levels=np.linspace(-30, 30, 100))

    # cb = plt.colorbar(cont, label="Kc1 (kJ/m3)")

    data = np.load('anisc2.npy')
    plot_quiver(data)
    
    plt.gca().set_aspect('equal')
    plt.show()
