import numpy as np
from matplotlib import pyplot as plt
from mumaxplot.io.parser import Parser
from mumaxplot.derivative import get_dmx_dt
from mumaxplot.folders import get_folders
from mumaxplot.io.table import loadtable
from mumaxplot.distributions.bursts import krms
from scipy.interpolate import interp1d


def avg_envelope():
    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    bigfolders = parser.bigfolders
    labels = parser.labels

    new_ts = np.linspace(0, 1800, 10000)
    new_Bs = np.linspace(-0.1, 0.1, 10000)

    for bfindex in range(len(bigfolders)):

        folders = get_folders(bigfolders[bfindex])
        ts, dms = [], []
        new_ms = np.zeros_like(new_ts)

        for findex, folder in enumerate(folders):
            t, mx, m, B = loadtable('{:s}/calc.out/table.txt'.format(folder),
                                    usecols=(0, 1, parser.m_index(), 4))

            if t is None:
                continue

            t *= 1e9  # ns

            if mx[-1] > parser.finishlimit:
                print(folder, "not finished. "
                      "(Last magnetization at {:.02f} ns: {:.02f})"
                      .format(t[-1], mx[-1]))
                continue

            # Trim zero times:
            mask = np.nonzero(t)
            t = t[mask]
            m = m[mask]
            B = B[mask]

            # Get signal:
            dmx_dt = get_dmx_dt(t, m, absv=True).reshape(-1)
            B = B[:-1]

            # Interpolate to matching grids:
            f = interp1d(B, dmx_dt, bounds_error=False, fill_value=0.)
            interpolated = f(new_Bs)

            # k-RMS:
            interpolated = krms(interpolated, k=parser.kmeans_k).reshape(-1)

            new_ms += interpolated

        new_ms /= len(folders)
        plt.plot(new_Bs, new_ms, label=parser.labels[bfindex], linewidth=3)

    plt.legend()
    plt.show()
