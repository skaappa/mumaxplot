import numpy as np
from mumaxplot.kmeans import kmean
from matplotlib import pyplot as plt

def get_dmx_dt(t, mx, absv=False, subtract_median=False,
               median_window=2000):
    dmx_dt = np.diff(mx) / np.diff(t)

    if subtract_median and median_window > 0:
        t = t[:-1]
        dt = t[1] - t[0]
        if not np.allclose(dt, np.diff(t), atol=0.001):
            raise RuntimeError("Time steps not evenly spaced!")

        median_window_bins = int(median_window / dt)

        median = kmean(dmx_dt, k=median_window_bins, median=True)
        dmx_dt -= median

    if absv:
        dmx_dt = np.abs(dmx_dt)

    return dmx_dt
