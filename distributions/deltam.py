import numpy as np
from matplotlib import pyplot as plt
from mumaxplot.io.parser import Parser
from mumaxplot.io.table import loadtable
from mumaxplot.folders import get_folders
from mumaxplot.fit.functions import (fit_cut_powerlaw, get_fit_llim_idx,
                                     plot_fitarea)
from mumaxplot.derivative import get_dmx_dt
from mumaxplot.histogram import (strip_too_small_elements, get_nbins, get_bins,
                                 get_bincenters, get_histogram)
import warnings

plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'


def get_peaks_single_folder(parser, folder, dmx_llim=None):

    if dmx_llim is None:
        dmx_llim = parser.dmx_llim

    t, m0, mx, B = loadtable('{:s}/calc.out/table.txt'.format(folder),
                              usecols=(0, 1, parser.m_index(), 4))
    if t is None:
        return

    B *= 1e3  # milliteslas

    if m0[-1] > parser.finishlimit:
        print(folder, "not finished. "
              "(Last magnetization at {:.02f} mT: {:.02f})".format(B[-1],
                                                                   m0[-1]))
        return

    # Resolve gradient:
    dmx_dB = np.diff(mx) / np.diff(B)


    dmx_dB = dmx_dB[B[1:] < 0.98]  # <------------------ NOTE

    dmx_dB = np.abs(dmx_dB)
    dmx_dB = dmx_dB[np.isfinite(dmx_dB)]
    print(folder)

    return dmx_dB


def get_peaks(parser):

    bigfolders = parser.bigfolders
    dmx_llim = parser.dmx_llim if parser.dmx_llim is not None else 0.

    if dmx_llim > 0.0:
        print("Excluding peaks with height < {:.02E}".format(dmx_llim))

    peak_min = np.inf  # initialize
    peak_max = 0.0  # initialize

    # Read data and convert to peaks:
    for bfindex, bigfolder in enumerate(bigfolders):

        folders = get_folders(bigfolder)
        bigfolder_peaks = []
        for findex, folder in enumerate(folders):

            if not parser.reread:
                bigfolder_peaks = np.load('peaks_{:d}.npy'.format(bfindex))
                break

            peaks = get_peaks_single_folder(parser, folder, dmx_llim)

            if peaks is None:
                continue

            peaks = peaks[np.nonzero(peaks)]

            bigfolder_peaks += list(peaks)

            if parser.printfolders:
                print(folder)

        np.save('peaks_{:d}.npy'.format(bfindex), np.array(bigfolder_peaks))

        peak_min_bf = np.min(bigfolder_peaks)
        peak_max_bf = np.max(bigfolder_peaks)
        if peak_min_bf < peak_min:
            peak_min = peak_min_bf
        if peak_max_bf > peak_max:
            peak_max = peak_max_bf

    return peak_min, peak_max


def qs_bndist():
    '''
    Quasistatic Barkhausen noise peak distribution.
    '''

    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    bigfolders = parser.bigfolders
    labels = parser.labels
    binwidth = parser.binwidth if parser.binwidth is not None else 0.25
    fit = parser.fit
    fit_llim = parser.fit_llim if parser.fit_llim is not None else 2.0e-5

    peak_min, peak_max = get_peaks(parser)

    nbins = get_nbins(peak_min, peak_max, binwidth)
    bins = get_bins(peak_min, peak_max, nbins)

    # Get bin centers for fit and plotting:
    bincenters = get_bincenters(bins)

    for bfindex in range(len(bigfolders)):

        # Calculate histogram:
        bigfolder_peaks = list(np.load('peaks_{:d}.npy'.format(bfindex)))
        hist = get_histogram(bigfolder_peaks, bins)

        # Scale for readability
        scalefactor = 100**(-(len(bigfolders) - 1 - bfindex))

        # Fit curve:
        if fit:
            tau, s0, prefactor, perr = fit_cut_powerlaw(bincenters, hist,
                                                        fit_llim,
                                                        scalefactor,
                                                        label=bigfolders[bfindex])

        # Plot histogram:
        bflabel = labels[bfindex]

        if fit:
            bflabel += r" ($\tau$={:.02f}, $s_0$={:.02E})".format(tau, s0)

        plt.loglog(bincenters,
                   hist * scalefactor, linestyle='none', marker='o',
                   linewidth=4, markersize=8,
                   label=bflabel)

    if parser.xlim is not None:
        plt.xlim(parser.xlim)
    elif fit:
        plt.xlim((fit_llim * 0.1, peak_max * 2))

    if parser.ylim is not None:
        plt.ylim(parser.ylim)

    # Fill area for fitted data region:
    if fit:
        plot_fitarea(bincenters, fit_llim)

    # Inverse legend order:
    handles, labels = plt.gca().get_legend_handles_labels()
    handles = handles[::-1]
    labels = labels[::-1]

    if fit:
        handles.append(handles.pop(0))
        labels.append(labels.pop(0))

    plt.legend(handles, labels)

    m_label = parser.m_label()
    plt.xlabel("dm$_{:s}$/dB (mT$^{{-1}}$)".format(m_label))
    plt.ylabel("Probability (shifted)")

    if parser.savepng:
        fname = "dm{:s}_distribution.png".format(m_label)
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    if parser.showfig:
        plt.show()
