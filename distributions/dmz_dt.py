import numpy as np
from matplotlib import pyplot as plt
from mumaxplot.io.parser import Parser
from mumaxplot.io.table import loadtable
from mumaxplot.folders import get_folders
from mumaxplot.fit.functions import (fit_cut_powerlaw, get_fit_llim_idx,
                                     plot_fitarea)
from mumaxplot.derivative import get_dmx_dt
from mumaxplot.histogram import (strip_too_small_elements, get_nbins, get_bins,
                                 get_bincenters, get_histogram)
import warnings

plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'


def get_peaks_single_folder(parser, folder, dmx_llim=None):

    if dmx_llim is None:
        dmx_llim = parser.dmx_llim

    t, m0, mx = loadtable('{:s}/calc.out/table.txt'.format(folder),
                         usecols=(0, 1, parser.m_index()))
    if t is None:
        return

    t *= 1e9  # ns

    if m0[-1] > parser.finishlimit:
        print(folder, "not finished. "
              "(Last magnetization at {:.02f} ns: {:.02f})".format(t[-1],
                                                                   m0[-1]))
        return

    # Resolve gradient:
    if parser.positive is not None:
        dmx_dt = get_dmx_dt(t, mx, absv=False).flatten()
        if not parser.positive:
            dmx_dt *= -1.0

        dmx_dt = dmx_dt[np.argwhere(np.diff(t) != 0)]
        t = t[:-1][np.argwhere(np.diff(t) != 0)]
        t = t[dmx_dt > 0.]
        dmx_dt = dmx_dt[dmx_dt > 0.]

    else:  # use absolute value of the signal
        dmx_dt = get_dmx_dt(t, mx, absv=True).flatten()
        dmx_dt = dmx_dt[np.argwhere(np.diff(t) != 0)] 
        t = t[:-1]

    # Resolve peaks:
    peaks = strip_too_small_elements(dmx_dt, dmx_llim)

    return peaks

def get_peaks(parser, bigfolders):

    dmx_llim = parser.dmx_llim if parser.dmx_llim is not None else 0.

    if dmx_llim > 0.0:
        print("Excluding peaks with height < {:.02E}".format(dmx_llim))

    peak_min = np.inf  # initialize
    peak_max = 0.0  # initialize

    # Read data and convert to peaks:
    for bfindex, bigfolder in enumerate(bigfolders):

        folders = get_folders(bigfolder)
        bigfolder_peaks = []
        for findex, folder in enumerate(folders):

            if not parser.reread:
                bigfolder_peaks = np.load('peaks_{:d}.npy'.format(bfindex))
                break

            peaks = get_peaks_single_folder(parser, folder, dmx_llim)

            if peaks is None:
                continue

            bigfolder_peaks += list(peaks)

            if parser.printfolders:
                print(folder)

        np.save('peaks_{:d}.npy'.format(bfindex), np.array(bigfolder_peaks))

        peak_min_bf = np.min(bigfolder_peaks)
        peak_max_bf = np.max(bigfolder_peaks)
        if peak_min_bf < peak_min:
            peak_min = peak_min_bf
        if peak_max_bf > peak_max:
            peak_max = peak_max_bf

    return peak_min, peak_max


def dmz_dt():

    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    bigfolders = parser.bigfolders
    labels = parser.labels
    binwidth = parser.binwidth if parser.binwidth is not None else 0.25
    fit = parser.fit
    fit_llim = parser.fit_llim if parser.fit_llim is not None else 2.0e-5

    peak_min, peak_max = get_peaks(parser, bigfolders)

    nbins = get_nbins(peak_min, peak_max, binwidth)
    bins = get_bins(peak_min, peak_max, nbins)

    # Get bin centers for fit and plotting:
    bincenters = get_bincenters(bins)

    for bfindex in range(len(bigfolders)):

        # Calculate histogram:
        bigfolder_peaks = list(np.load('peaks_{:d}.npy'.format(bfindex)))
        hist = get_histogram(bigfolder_peaks, bins)

        # Scale for readability
        scalefactor = 100**(-(len(bigfolders) - 1 - bfindex))

        # Fit curve:
        if fit:
            tau, s0, prefactor, perr = fit_cut_powerlaw(bincenters, hist,
                                                        fit_llim,
                                                        scalefactor,
                                                        label=bigfolders[bfindex])

        # Plot histogram:
        bflabel = labels[bfindex]

        if fit:
            bflabel += r" ($\tau$={:.02f}, $s_0$={:.02E})".format(tau, s0)

        plt.loglog(bincenters,
                   hist * scalefactor, linestyle='none', marker='o',
                   linewidth=4, markersize=8,
                   label=bflabel)

    if parser.xlim is not None:
        plt.xlim(parser.xlim)
    elif fit:
        plt.xlim((fit_llim * 0.1, peak_max * 2))

    if parser.ylim is not None:
        plt.ylim(parser.ylim)

    # Fill area for fitted data region:
    if fit:
        plot_fitarea(bincenters, fit_llim)

    # Inverse legend order:
    handles, labels = plt.gca().get_legend_handles_labels()
    handles = handles[::-1]
    labels = labels[::-1]

    if fit:
        handles.append(handles.pop(0))
        labels.append(labels.pop(0))

    plt.legend(handles, labels)

    m_label = parser.m_label()
    plt.xlabel("$s\equiv dm_{:s}/dt$ (1/ns)".format(m_label))
    plt.ylabel("Probability (shifted)")

    if parser.savepng:
        fname = "dm{:s}_dt_distribution.png".format(m_label)
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    if parser.showfig:
        plt.show()


def single_dm_dt_dist():

    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    folders = parser.folders
    labels = parser.labels
    binwidth = parser.binwidth if parser.binwidth is not None else 0.25
    fit = parser.fit
    fit_llim = parser.fit_llim if parser.fit_llim is not None else 2.0e-5
    dmx_llim = parser.dmx_llim if parser.dmx_llim is not None else 0.

    all_peaks = []
    for findex in range(len(folders)):
        folder = folders[findex]
        peaks = get_peaks_single_folder(parser, folder, dmx_llim)
        all_peaks.append(peaks)

    peak_min = np.min(np.concatenate(all_peaks).flatten())
    peak_max = np.max(np.concatenate(all_peaks).flatten())

    nbins = get_nbins(peak_min, peak_max, binwidth)
    bins = get_bins(peak_min, peak_max, nbins)

    # Get bin centers for fit and plotting:
    bincenters = get_bincenters(bins)

    for findex in range(len(folders)):

        # Calculate histogram:
        peaks = all_peaks[findex]
        hist = get_histogram(peaks, bins)

        # Scale for readability
        scalefactor = 100**(-(len(folders) - 1 - findex))

        # Fit curve:
        if fit:
            tau, s0, prefactor, perr = fit_cut_powerlaw(bincenters, hist,
                                                        fit_llim,
                                                        scalefactor,
                                                        label=folders[findex])

        # Plot histogram:
        flabel = labels[findex]

        if fit:
            flabel += r" ($\tau$={:.02f}, $s_0$={:.02E})".format(tau, s0)

        plt.loglog(bincenters,
                   hist * scalefactor, linestyle='none', marker='o',
                   linewidth=4, markersize=8,
                   label=flabel)

    if parser.xlim is not None:
        plt.xlim(parser.xlim)
    elif fit:
        plt.xlim((fit_llim * 0.1, peak_max * 2))

    if parser.ylim is not None:
        plt.ylim(parser.ylim)

    # Fill area for fitted data region:
    if fit:
        plot_fitarea(bincenters, fit_llim)

    # Inverse legend order:
    handles, labels = plt.gca().get_legend_handles_labels()
    handles = handles[::-1]
    labels = labels[::-1]

    if fit:
        handles.append(handles.pop(0))
        labels.append(labels.pop(0))

    plt.legend(handles, labels)

    m_label = parser.m_label()
    plt.xlabel("$s\equiv dm_{:s}/dt$ (1/ns)".format(m_label))
    plt.ylabel("Probability (shifted)")

    if parser.savepng:
        fname = "dm{:s}_dt_distribution.png".format(m_label)
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    if parser.showfig:
        plt.show()


def dmz_dt_pos_neg():

    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    bigfolders = parser.bigfolders
    labels = parser.labels
    binwidth = parser.binwidth if parser.binwidth is not None else 0.25
    fit = parser.fit
    fit_llim = parser.fit_llim if parser.fit_llim is not None else 2.0e-5

    peak_min, peak_max = get_peaks_pos_neg(parser)

    nbins = get_nbins(peak_min, peak_max, binwidth)
    bins = get_bins(peak_min, peak_max, nbins)

    # Get bin centers for fit and plotting:
    bincenters = get_bincenters(bins)

    for factorindex, factor in enumerate([1, -1]):

        # Calculate histogram:
        bigfolder_peaks = list(np.load('peaks_{:d}.npy'.format(factorindex)))
        hist = get_histogram(bigfolder_peaks, bins)

        # Scale for readability
        scalefactor = 100**(-(len(bigfolders) - 1 - factorindex))

        label = ["Positive", "Negative"][factorindex]
        
        # Fit curve:
        if fit:
            tau, s0, prefactor, perr = fit_cut_powerlaw(bincenters, hist,
                                                        fit_llim,
                                                        scalefactor,
                                                        label=label)

        # Plot histogram:

        if fit:
            label += r" ($\tau$={:.02f}, $s_0$={:.02E})".format(tau, s0)

        plt.loglog(bincenters,
                   hist * scalefactor, linestyle='none', marker='o',
                   linewidth=4, markersize=8,
                   label=label)

    if parser.xlim is not None:
        plt.xlim(parser.xlim)
    elif fit:
        plt.xlim((fit_llim * 0.1, peak_max * 2))

    if parser.ylim is not None:
        plt.ylim(parser.ylim)

    # Fill area for fitted data region:
    if fit:
        plot_fitarea(bincenters, fit_llim)

    # Inverse legend order:
    handles, labels = plt.gca().get_legend_handles_labels()
    handles = handles[::-1]
    labels = labels[::-1]

    if fit:
        handles.append(handles.pop(0))
        labels.append(labels.pop(0))

    plt.legend(handles, labels)

    m_label = parser.m_label()
    plt.xlabel("$s\equiv dm_{:s}/dt$ (1/ns)".format(m_label))
    plt.ylabel("Probability (shifted)")

    if parser.savepng:
        fname = "dm{:s}_dt_pos_neg.png".format(m_label)
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    if parser.showfig:
        plt.show()


def get_peaks_pos_neg(parser):

    dmx_llim = parser.dmx_llim if parser.dmx_llim is not None else 0.

    if dmx_llim > 0.0:
        print("Excluding peaks with height < {:.02E}".format(dmx_llim))

    peak_min = np.inf  # initialize
    peak_max = 0.0  # initialize

    # Read data and convert to peaks:
    for factorindex, factor in enumerate([1, -1]):

        parser.options.positive = not bool(factorindex)  # hack

        folders = get_folders('.')
        bigfolder_peaks = []
        for findex, folder in enumerate(folders):

            if not parser.reread:
                bigfolder_peaks = np.load('peaks_{:d}.npy'.format(factorindex))
                break

            peaks = get_peaks_single_folder(parser, folder, dmx_llim)

            if peaks is None:
                continue

            bigfolder_peaks += list(peaks)

            if parser.printfolders:
                print(folder)

        np.save('peaks_{:d}.npy'.format(factorindex), np.array(bigfolder_peaks))

        peak_min_bf = np.min(bigfolder_peaks)
        peak_max_bf = np.max(bigfolder_peaks)
        if peak_min_bf < peak_min:
            peak_min = peak_min_bf
        if peak_max_bf > peak_max:
            peak_max = peak_max_bf

    return peak_min, peak_max
