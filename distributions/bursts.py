import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import linregress
from mumaxplot.io.parser import Parser
from mumaxplot.folders import get_folders
from mumaxplot.fit.functions import (fit_cut_powerlaw, get_fit_llim_idx,
                                     plot_fitarea)
from mumaxplot.derivative import get_dmx_dt
from mumaxplot.histogram import (strip_too_small_elements, get_nbins, get_bins,
                                 get_bincenters, get_histogram)
from mumaxplot.io.table import loadtable
from mumaxplot.kmeans import kmean, krms
import warnings
import time

plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'


def localmaxima(signal):
    maxima_loc = []
    for i in range(1, len(signal) - 1):
        if signal[i - 1] < signal[i] and signal[i] > signal[i + 1]:
            maxima_loc.append(i)
    maxima_val = signal[maxima_loc]
    return maxima_loc, maxima_val


def get_areas_bymaxima(t, signal, threshold, plot_signal=False):

    assert len(t) == len(signal)

    areas = []
    areatimes = []
    start_times, end_times = [], []
    current_area = 0.0

    # signal = kmean(signal, k=40)
    # index, signal = localmaxima(signal)
    # t = t[index]

    in_burst = False
    for i in range(1, len(t) - 1):

        record = signal[i] > threshold

        is_localmaximum = (signal[i - 1] < signal[i] and
                           signal[i] > signal[i + 1])

        if record and not in_burst and is_localmaximum: # signal[i - 1] < threshold:  # start of segment
            start_time = t[i]
            start_times.append(start_time)
            in_burst = True

        # The first element of signal might be larger than threshold.
        # Lets avoid errors due to that:
        if not in_burst:
            continue

        if in_burst and record:
            current_area += (signal[i] - threshold) * (t[i] - t[i - 1])

        if in_burst and is_localmaximum and signal[i + 1] < threshold:  # end of segment
            end_time = t[i]
            end_times.append(end_time)
            dt = end_time - start_time
            if dt != 0.0:
                areas.append(current_area)
                areatimes.append(dt)
            current_area = 0.0
            in_burst = False

    # Plot signal sample:
    areas = np.array(areas)
    areatimes = np.array(areatimes)

    if plot_signal:  # not (areas >= 0.).all() or not (areatimes >= 0.).all():

        plt.rcParams['figure.figsize'] = 16, 4
        plt.plot(t, signal)
        for i in range(len(start_times)):
            plt.axvspan(start_times[i], end_times[i], color='green', alpha=0.1)

        plt.axhline(0.0, color='k', linestyle='--')
        plt.axhline(threshold, color='k', linestyle='--')

        plt.xlabel('t (ns)')
        plt.ylabel('|dmz/dt|')
        plt.xlim((0.95 / 2 * max(t), 1.2 / 2 * max(t)))
        # plt.ylim((-0.005, 0.0275))

        plt.savefig('signal_example.png', bbox_inches='tight', dpi=200)

        plt.show()
        exit()

    assert len(areas) == len(areatimes)

    return areas, areatimes


def get_areas_byrunningrms(t, signal, threshold, k=10,
                           plot_signal=False):

    # preprocess:
    if k != 1:
        signal = krms(signal=signal, k=k)

    return get_areas(t, signal, threshold, plot_signal)


def get_areas(t, signal, threshold, plot_signal=True):

    assert len(t) == len(signal)

    areas = []
    areatimes = []
    start_times, end_times = [], []
    current_area = 0.0

    # signal = kmean(signal, k=40)

    # index, signal = localmaxima(signal)
    # t = t[index]

    for i in range(1, len(t) - 1):

        record = signal[i] > threshold

        if record:

            if signal[i - 1] < threshold:  # start of segment
                start_time = t[i]
                start_times.append(start_time)

            # The first element of signal might be larger than threshold.
            # Lets avoid errors due to that:
            if 'start_time' not in locals():
                continue

            current_area += (signal[i] - threshold) * (t[i] - t[i - 1])

            if signal[i + 1] < threshold:  # end of segment
                end_time = t[i]
                end_times.append(end_time)
                dt = end_time - start_time
                if dt != 0.0:
                    areas.append(current_area)
                    areatimes.append(dt)
                current_area = 0.0

    # Plot signal sample:
    areas = np.array(areas)
    areatimes = np.array(areatimes)

    if plot_signal:

        plt.rcParams['figure.figsize'] = 16, 4
        plt.plot(t, signal)
        for i in range(len(start_times)):
            plt.axvspan(start_times[i], end_times[i], color='green', alpha=0.1)

        plt.axhline(0.0, color='k', linestyle='--')
        plt.axhline(threshold, color='k', linestyle='--')

        plt.xlabel('t (ns)')
        plt.ylabel('|dmz/dt|')
        plt.xlim((0.95 / 2 * max(t), 1.2 / 2 * max(t)))
        # plt.ylim((-0.005, 0.0275))

        plt.savefig('signal_example.png', bbox_inches='tight', dpi=200)

        plt.show()
        exit()

    assert len(areas) == len(areatimes)

    return areas, areatimes


def get_areadata(parser, bigfolders, datatype='sizes'):

    m_index = parser.m_index()
    dmx_llim = parser.dmx_llim if parser.dmx_llim is not None else 0.
    threshold = parser.threshold if parser.threshold is not None else 3e-3

    if dmx_llim > 0.0:
        print("Excluding peaks with height < {:.02E}".format(dmx_llim))

    peak_min = np.inf  # initialize
    peak_max = 0.0  # initialize

    # Read data and convert to peaks:
    for bfindex, bigfolder in enumerate(bigfolders):

        folders = get_folders(bigfolder)
        bigfolder_sizes = []
        bigfolder_times = []
        for findex, folder in enumerate(folders):

            if not parser.reread:
                bigfolder_sizes = np.load('sizes_{:d}.npy'.format(bfindex))
                bigfolder_times = np.load('times_{:d}.npy'.format(bfindex))
                break

            # Get data:
            # t, mx = np.loadtxt('{:s}/calc.out/table.txt'.format(folder),
            #                     usecols=(0, m_index)).T

            t, m0, mx = loadtable('{:s}/calc.out/table.txt'.format(folder),
                              usecols=(0, 1, m_index))
            if t is None:
                continue

            nonzero = np.nonzero(t)
            t = t[nonzero]
            mx = mx[nonzero]

            t *= 1e9  # ns

            if m0[-1] > parser.finishlimit:
                print(folder, "not finished. "
                      "(Last time step at {:.02f} ns)".format(t[-1]))
                continue

            # Resolve gradient:
            if parser.positive is not None:
                dmx_dt = get_dmx_dt(t, mx, absv=False, subtract_median=True,
                                    median_window=parser.median_window).flatten()
                if not parser.positive:
                     dmx_dt *= -1.0

                dmx_dt = dmx_dt[np.argwhere(np.diff(t) != 0)]
                t = t[:-1][np.argwhere(np.diff(t) != 0)]
                t = t[dmx_dt > 0.]
                dmx_dt = dmx_dt[dmx_dt > 0.]

            else:  # use absolute value of the signal
                dmx_dt = get_dmx_dt(t, mx, absv=True, subtract_median=True,
                                    median_window=parser.median_window).flatten()
                dmx_dt = dmx_dt[np.argwhere(np.diff(t) != 0)] 
                t = t[:-1]

            # Resolve peaks:
            # areas, areatimes = get_areas_bymaxima(t[:-1], dmx_dt, threshold)
            areas, areatimes = get_areas_byrunningrms(t, dmx_dt,
                                                      threshold,
                                                      k=parser.kmeans_k,
                                                      plot_signal=False)
            # areas, areatimes = get_areas(t[:-1], dmx_dt, threshold,
            #                              plot_signal=False)

            bigfolder_sizes += list(areas)
            bigfolder_times += list(areatimes)

            if parser.printfolders:
                print(folder)

        np.save('sizes_{:d}.npy'.format(bfindex), np.array(bigfolder_sizes))
        np.save('times_{:d}.npy'.format(bfindex), np.array(bigfolder_times))

        if datatype == 'sizes':
            bigfolder_peaks = bigfolder_sizes
        elif datatype == 'times':
            bigfolder_peaks = bigfolder_times

        peak_min_bf = np.min(bigfolder_peaks)
        peak_max_bf = np.max(bigfolder_peaks)
        if peak_min_bf < peak_min:
            peak_min = peak_min_bf
        if peak_max_bf > peak_max:
            peak_max = peak_max_bf

    return peak_min, peak_max


def areasizes(datatype='sizes'):

    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    bigfolders = parser.bigfolders
    labels = parser.labels
    binwidth = parser.binwidth if parser.binwidth is not None else 0.25
    fit = parser.fit
    fit_llim = parser.fit_llim if parser.fit_llim is not None else 2.0e-5

    peak_min, peak_max = get_areadata(parser, bigfolders, datatype=datatype)

    nbins = get_nbins(peak_min, peak_max, binwidth)
    bins = get_bins(peak_min, peak_max, nbins)

    # Get bin centers for fit and plotting:
    bincenters = get_bincenters(bins)

    for bfindex in range(len(bigfolders)):

        # Calculate histogram:
        bigfolder_peaks = list(np.load('{:s}_{:d}.npy'.format(datatype,
                                                              bfindex)))
        hist = get_histogram(bigfolder_peaks, bins)

        # Scale for readability
        scalefactor = 100**(-(len(bigfolders) - 1 - bfindex))

        # Fit curve:
        if fit:
            tau, s0, prefactor, perr = fit_cut_powerlaw(bincenters, hist,
                                                        fit_llim,
                                                        scalefactor,
                                                        label=labels[bfindex])

        # Plot histogram:
        bflabel = labels[bfindex]

        if fit:
            bflabel += r" ($\tau$={:.02f}, $s_0$={:.02E})".format(tau, s0)

        plt.loglog(bincenters,
                   hist * scalefactor, linestyle='none', marker='o',
                   linewidth=4, markersize=8,
                   label=bflabel)

    if parser.xlim is not None:
        plt.xlim(parser.xlim)
    elif fit:
        plt.xlim((max(peak_min, fit_llim) * 0.1, peak_max * 2))

    if parser.ylim is not None:
        plt.ylim(parser.ylim)

    # Fill area for fitted data region:
    if fit:
        plot_fitarea(bincenters, fit_llim)

    # Inverse legend order:
    handles, labels = plt.gca().get_legend_handles_labels()
    handles = handles[::-1]
    labels = labels[::-1]

    if fit:
        handles.append(handles.pop(0))
        labels.append(labels.pop(0))

    plt.legend(handles, labels)

    m_label = parser.m_label()

    labeldict = {'sizes': "S",
                 'times': "T (ns)"}
    plt.xlabel(labeldict[datatype])
    plt.ylabel("Probability (shifted)")

    if parser.savepng:
        fname = "area{:s}_{:s}.png".format(datatype, m_label)
        plt.savefig(fname, dpi=200, bbox_inches='tight',
                    metadata={'samis text': "samis test"})

    if parser.showfig:
        plt.show()


def areatimes():

    areasizes(datatype='times')


def get_S_means(sizes, times, parser):
    nbins = get_nbins(min(times), max(times), parser.binwidth)
    bins = get_bins(min(times), max(times), nbins=nbins)
    Smeans = np.zeros(len(bins) - 1)
    for i in range(len(bins) - 1):
        nS = 0
        for j in range(len(sizes)):
            if bins[i] < times[j] < bins[i + 1]:
                Smeans[i] += sizes[j]
                nS += 1

        if nS != 0:
            Smeans[i] /= nS

    bincenters = get_bincenters(bins)
    return bincenters, Smeans


def sizetime():

    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    bigfolders = parser.bigfolders
    labels = [str(label) for label in parser.labels]
    fit = parser.fit
    binwidth = parser.binwidth if parser.binwidth is not None else 0.25
    fit_llim = parser.fit_llim if parser.fit_llim is not None else 2.0e-5

    # Save data to .npy files:
    if parser.reread:
        get_areadata(parser, bigfolders)

    for bfindex, bigfolder in enumerate(bigfolders):

        areasizes = np.load('sizes_{:d}.npy'.format(bfindex))
        areatimes = np.load('times_{:d}.npy'.format(bfindex))

        bins, Smeans = get_S_means(areasizes, areatimes, parser)

        Smeans *= 10**bfindex

        plt.loglog(bins, Smeans,
                   linestyle='none', marker='o', markersize=8,
                   label=labels[bfindex])

        if fit:
            y = Smeans
            nonzero = np.argwhere(y > 0)
            x = bins[nonzero].flatten()
            y = y[nonzero].flatten()

            if parser.fit_llim is not None:
                limit_idx = get_fit_llim_idx(x, parser.fit_llim)

                x = x[limit_idx:]
                y = y[limit_idx:]

            # do fitting:
            slope, intercept, r, p, se = linregress(np.log10(x), np.log10(y))

            # plot fitted curve:
            xx = np.logspace(np.log10(min(x)), np.log10(max(x)), 3)

            plt.plot(xx,
                     10**(slope * np.log10(xx) + intercept),
                     'k--',
                      zorder=-1)

            # Print curve fit:
            print("{:s}\t tau: {:.04f}"
                  .format(labels[bfindex], slope))

            labels[bfindex] += " (Slope: {:.02f})".format(slope)


    if parser.xlim is not None:
        plt.xlim(parser.xlim)

    if parser.ylim is not None:
        plt.ylim(parser.ylim)

    handles, _ = plt.gca().get_legend_handles_labels()
    handles = handles[::-1]
    labels = labels[::-1]
    plt.legend(handles, labels)

    plt.xlabel("T (ns)")
    plt.ylabel("<S>")

    m_label = parser.m_label()

    if parser.savepng:
        fname = "sizetime_{:s}.png".format(m_label)
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    if parser.showfig:
        plt.show()


def sizetime_multithreshold():

    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    bigfolders = parser.bigfolders
    fit = parser.fit
    fit_llim = parser.fit_llim if parser.fit_llim is not None else 2.0e-5

    thresholds = parser.thresholds
    labels = [str(label) for label in parser.thresholds]

    for tindex, threshold in enumerate(thresholds):

        parser.options.threshold = threshold  # tweak

        # Save data to .npy fiels:
        if parser.reread:
            get_areadata(parser, bigfolders)

        bfindex = 0

        areasizes = np.load('sizes_{:d}.npy'.format(bfindex))
        areatimes = np.load('times_{:d}.npy'.format(bfindex))

        bins, Smeans = get_S_means(areasizes, areatimes, parser)

        plt.loglog(bins, Smeans,
                   linestyle='none', marker='o',
                   label=threshold)

        if fit:
            y = Smeans
            nonzero = np.argwhere(y > 0)
            x = bins[nonzero].flatten()
            y = y[nonzero].flatten()

            # do fitting:
            slope, intercept, r, p, se = linregress(np.log10(x), np.log10(y))

            # plot fitted curve:
            xx = np.logspace(np.log10(min(x)), np.log10(max(x)), 3)

            plt.plot(xx,
                     10**(slope * np.log10(xx) + intercept),
                     'k--',
                      zorder=-1)

            # Print curve fit:
            print("{:s}    tau: {:.04f}"
                  .format(labels[tindex], slope))

            labels[tindex] += " (Slope: {:.02f})".format(slope)


    if parser.xlim is not None:
        plt.xlim(parser.xlim)

    if parser.ylim is not None:
        plt.ylim(parser.ylim)

    handles, _ = plt.gca().get_legend_handles_labels()
    handles = handles[::-1]
    labels = labels[::-1]
    print(labels)
    plt.legend(handles, labels, title='Threshold')

    plt.xlabel("T (ns)")
    plt.ylabel("<S> ")

    m_label = parser.m_label()

    if parser.savepng:
        fname = "sizetime_multithreshold_{:s}.png".format(m_label)
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    if parser.showfig:
        plt.show()
