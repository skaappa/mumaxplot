def read(fname):
    '''
    Read header from .ovf file. Read the file in binary form
    and decode to utf8 along the way.
    '''
    with open(fname, 'rb') as f:
        lines = []
        for line in f:
            row = line.split()

            if row[1].decode('utf-8').startswith('End:'):
                break

            lines.append(line.decode('utf-8'))
    return lines

def get_headerdata(fname='m.ovf'):

    lines = read(fname)

    headerdata = {}
    keys = ['xmin', 'ymin', 'zmin',
            'xmax', 'ymax', 'zmax',
            'xnodes', 'ynodes', 'znodes',
            'xstepsize', 'ystepsize', 'zstepsize']
    for line in lines:
        row = line.split()

        for key in keys:
            if row[1].startswith(key + ':'):
                headerdata.update({key: float(row[2])})

    return headerdata
