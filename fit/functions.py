import numpy as np
from matplotlib import pyplot as plt
import matplotlib.transforms as mtransforms
from scipy.optimize import curve_fit


def P(s, tau, s0, prefactor):
    return prefactor * s**(-tau) * np.exp(-s / s0)


def logP(s, tau, s0, prefactor):
    return np.log10(prefactor) - tau * np.log10(s) - s / (s0 * np.log(10))


def logP_nocut(s, tau, prefactor):
    return np.log10(prefactor) - tau * np.log10(s)


def get_fit_llim_idx(bins, limit):
    return (np.abs(bins - limit)).argmin()


def fit_cut_powerlaw(x, y, limit, scalefactor=1.0, label=""):

    limit_idx = get_fit_llim_idx(x, limit)

    x = np.array(x)[limit_idx:]
    y = np.array(y)[limit_idx:]
    nonzero = np.argwhere(y > 0)
    x = x[nonzero].flatten()
    y = np.log10(y[nonzero]).flatten()

    # do fitting:
    y = y + 12
    (tau, s0, prefactor), pcov = curve_fit(logP, x, y,
                                           p0 = [1.3, 0.4, 0.],
                                           ftol=1e-8,
                                           xtol=1e-8,
                                           gtol=1e-8,
                                           loss='linear',
                                           max_nfev=10000,
                                           bounds=np.array(([0., np.inf],
                                                            [0., np.inf],
                                                            [0., np.inf])).T)

    # 1-sigma error estimates:
    perr = np.sqrt(np.diag(pcov))

    prefactor *= 1e-12
    # plot fitted curve:
    xx = np.logspace(np.log10(min(x)), np.log10(max(x)), 40)
    plt.plot(xx,
             np.power(10, logP(xx, tau, s0, prefactor)) * scalefactor,
             'k--',
             zorder=-1)

    # Calculate error of the fit:
    fit_residual_error = np.sqrt(np.mean(np.square(np.power(10, logP(x, tau, s0,
                                                                     prefactor))
                                                   - y)))

    # Print curve fit:
    print("{:s}\t tau: {:.04f} ({:.03f})   "
          "s0: {:.02E} ({:.02E})   "
          "prefactor: {:.04E}    error: {:.04f}"
          .format(label, tau,
                  perr[0], s0,
                  perr[1], prefactor, fit_residual_error))

    return tau, s0, prefactor, perr


def plot_fitarea(x, limit, ulim=None, add_text=True):

    limit_idx = get_fit_llim_idx(x, limit)
    ulim_idx = get_fit_llim_idx(x, ulim) if ulim is not None else len(x) - 1

    trans = mtransforms.blended_transform_factory(plt.gca().transData,
                                                  plt.gca().transAxes)

    llim = x[limit_idx]
    ulim = x[ulim_idx]

    plt.gca().fill_between([llim, ulim+1], 0, 1,
                           facecolor='green', alpha=0.2,
                           zorder=-1, transform=trans,
                           label="Data region for curve fit")

    # Add text about the fit function:
    if add_text:
        plt.text(0.05,  # 10**((np.log10(llim) + np.log10(ulim)) / 2),
                 0.05,
                 r"Fit function: $P(s) = k_0s^{-{\tau}}e^{-s/s_0}$",
                 transform=plt.gca().transAxes,  # trans,
                 verticalalignment='bottom',
                 horizontalalignment='left')  # 'center')


def fit_powerlaw(x, y, llim, ulim, scalefactor=1.0, label=""):

    # Index of x closest to llim:
    llim_idx = get_fit_llim_idx(x, llim)
    ulim_idx = get_fit_llim_idx(x, ulim)

    x = np.array(x)[llim_idx:ulim_idx+1]
    y = np.array(y)[llim_idx:ulim_idx+1]
    nonzero = np.argwhere(y > 0)
    x = x[nonzero].flatten()
    y = np.log10(y[nonzero]).flatten()

    # do fitting:
    y = y + 1
    (tau, prefactor), pcov = curve_fit(logP_nocut, x, y,
                                       p0 = [1.3, 0.],
                                       ftol=1e-8,
                                       xtol=1e-8,
                                       gtol=1e-8,
                                       loss='linear',
                                       max_nfev=10000,
                                       bounds=np.array(([0., np.inf],
                                                        [0., np.inf])).T)

    # 1-sigma error estimates:
    perr = np.sqrt(np.diag(pcov))

    prefactor *= 1e-1
    # plot fitted curve:
    xx = np.logspace(np.log10(min(x)), np.log10(max(x)), 40)
    plt.plot(xx,
             np.power(10, logP_nocut(xx, tau, prefactor)) * scalefactor,
             'k--',
             zorder=-1)

    # Calculate error of the fit:
    fit_residual_error = np.sqrt(np.mean(np.square(np.power(10,
                                            logP_nocut(x,
                                                       tau,
                                                       prefactor))
                                                   - y)))

    # Print curve fit:
    print("{:s}\t tau: {:.04f} ({:.03f})   "
          "prefactor: {:.04E}    error: {:.04f}"
          .format(label, tau,
                  perr[0],
                  prefactor, fit_residual_error))

    return tau, prefactor, perr
