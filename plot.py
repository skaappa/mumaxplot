from mumaxplot.io.parser import Parser
from mumaxplot.hysteresis import single_hysteresis, multihysteresis, thysteresis
from mumaxplot.distributions import (dmz_dt, areasizes, areatimes, sizetime,
                                     sizetime_multithreshold,
                                     single_dm_dt_dist, dmz_dt_pos_neg,
                                     qs_bndist)
from mumaxplot.single.dm_dt import single_dm_dt
from mumaxplot.single.deltam import qs_singlesignal, qs_singlesignal_dist
from mumaxplot.envelope import avg_envelope

from mumaxplot.plot2d.plot_m import initm
from mumaxplot.plot2d.plot_anist1 import anist1
from mumaxplot.plot2d.plot_regions import regions, Msat
from mumaxplot.plot2d.plot_anisc1 import anisc1


def run():
    parser = Parser()

    plottype = parser.plottype

    functions = {'hysteresis': single_hysteresis,
                 'multihysteresis': multihysteresis,
                 'dmz_dt': dmz_dt,
                 'areasizes': areasizes,
                 'areatimes': areatimes,
                 'sizetime': sizetime,
                 'sizetime_multithreshold': sizetime_multithreshold,
                 'single_dm_dt': single_dm_dt,
                 'single_dm_dt_dist': single_dm_dt_dist,
                 'envelope': avg_envelope,
                 'dmz_dt_pos_neg': dmz_dt_pos_neg,
                 'qs_bndist': qs_bndist,
                 'qs_singlesignal': qs_singlesignal,
                 'qs_singlesignal_dist': qs_singlesignal_dist,
                 'initm': initm,
                 'anist1': anist1,
                 'regions': regions,
                 'Msat': Msat,
                 'anisc1': anisc1,
                 'thysteresis': thysteresis}

    # Run:
    try:
        functions[plottype]()
    except KeyError:
        raise KeyError("Possible keys: {}".format((functions.keys())))
