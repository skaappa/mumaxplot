import numpy as np
from matplotlib import pyplot as plt
from mumaxplot.io.parser import Parser
from mumaxplot.io.table import loadtable
from mumaxplot.derivative import get_dmx_dt
from mumaxplot.kmeans import krms
from mumaxplot.histogram import (get_nbins, get_bins,
                                 get_bincenters, get_histogram)
from mumaxplot.fit.functions import (fit_cut_powerlaw, get_fit_llim_idx,
                                     plot_fitarea)
from mumaxplot.kmeans import kmean

plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'


def convert2milli(array):
    return 1.0e3 * array


def get_deltam(parser):

    m_index = parser.m_index()
    B_index = parser.B_index()

    # Read data and convert to peaks:
    t, mx, B = loadtable('table.txt',
                         usecols=(0, m_index, B_index))

    B = convert2milli(B)

    signal = np.diff(mx) / np.diff(B)

    signal = signal[mx[:-1] < 0.98]  # <----------------------- NOTE
    B = B[:-1][mx[:-1] < 0.98]

    return B, signal


def qs_singlesignal():

    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    x, signal = get_deltam(parser)

    # signal = krms(signal, k=parser.kmeans_k)

    if parser.log:
        signal = np.abs(signal)
        x = x[np.nonzero(signal)]
        signal = signal[np.nonzero(signal)]
        x = x[np.isfinite(signal)]
        signal = signal[np.isfinite(signal)]
        plt.semilogy(x, signal, 'b-', linewidth=2)
    else:
        plt.plot(x, signal, 'b-', linewidth=2)

    plt.axhline(0.0, linestyle='--', linewidth=1)

    if parser.threshold is not None:
        plt.axhline(parser.threshold, linestyle='--', linewidth=1)

    if parser.xlim is not None:
        plt.xlim(parser.xlim)

    if parser.ylim is not None:
        plt.ylim(parser.ylim)

    m_label = parser.m_label()
    plt.ylabel("$\Delta m$".format(m_label))
    plt.xlabel("B (mT)")

    if parser.savepng:
        fname = "deltam_{:s}.png".format(m_label)
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    if parser.showfig:
        plt.show()


def qs_singlesignal_dist():

    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    bigfolders = parser.bigfolders
    labels = parser.labels
    binwidth = parser.binwidth if parser.binwidth is not None else 0.25
    fit = parser.fit
    fit_llim = parser.fit_llim if parser.fit_llim is not None else 2.0e-5

    B, signal = get_deltam(parser)

    signal = np.abs(signal)
    signal = signal[np.nonzero(signal)]
    signal = signal[np.isfinite(signal)]

    peak_min, peak_max = np.min(signal), np.max(signal)

    nbins = get_nbins(peak_min, peak_max, binwidth)
    bins = get_bins(peak_min, peak_max, nbins)

    # Get bin centers for fit and plotting:
    bincenters = get_bincenters(bins)

    hist = get_histogram(signal, bins)

    # Fit curve:
    if fit:
        tau, s0, prefactor, perr = fit_cut_powerlaw(bincenters, hist,
                                                    fit_llim,
                                                    scalefactor=1,
                                                    label='')

    # Plot histogram:
    bflabel = ''

    if fit:
        bflabel += r" ($\tau$={:.02f}, $s_0$={:.02E})".format(tau, s0)

    plt.loglog(bincenters,
               hist, linestyle='none', marker='o',
               linewidth=4, markersize=8,
               label=bflabel)

    if parser.xlim is not None:
        plt.xlim(parser.xlim)
    elif fit:
        plt.xlim((fit_llim * 0.1, peak_max * 2))

    if parser.ylim is not None:
        plt.ylim(parser.ylim)

    # Fill area for fitted data region:
    if fit:
        plot_fitarea(bincenters, fit_llim)

    # Inverse legend order:
    handles, labels = plt.gca().get_legend_handles_labels()
    handles = handles[::-1]
    labels = labels[::-1]

    if fit:
        handles.append(handles.pop(0))
        labels.append(labels.pop(0))

    plt.legend(handles, labels)

    m_label = parser.m_label()
    plt.xlabel("$\Delta m_{:s}$ (1/mT)".format(m_label))
    plt.ylabel("Probability (shifted)")

    if parser.savepng:
        fname = "dm{:s}_distribution.png".format(m_label)
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    if parser.showfig:
        plt.show()
