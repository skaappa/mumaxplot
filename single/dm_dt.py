import numpy as np
from matplotlib import pyplot as plt
from mumaxplot.io.parser import Parser
from mumaxplot.io.table import loadtable
from mumaxplot.derivative import get_dmx_dt
from mumaxplot.kmeans import krms

plt.rcParams['font.size'] = 16
plt.rcParams['font.family'] = 'FreeSans'


def get_dm_dt(parser, absv=False, subtract_median=False):

    m_index = parser.m_index()

    # Read data and convert to peaks:
    t, mx = loadtable('table.txt',
                      usecols=(0, m_index))

    t *= 1e9  # ns

    nonzero = np.nonzero(t)
    t = t[nonzero]
    mx = mx[nonzero]

    # Resolve gradient:
    dmx_dt = get_dmx_dt(t, mx, absv=absv,
                        subtract_median=subtract_median,
                        median_window=parser.median_window)

    return t[:-1], dmx_dt


def single_dm_dt():

    parser = Parser()

    plt.rcParams['figure.figsize'] = (parser.figsize
                                      if parser.figsize is not None
                                      else (10, 10))

    x, signal = get_dm_dt(parser)  # , absv=True, subtract_median=True)

    # signal = krms(signal, k=parser.kmeans_k)

    if parser.log:
        plt.semilogy(x, signal, 'b-', linewidth=2)
    else:
        plt.plot(x, signal, 'b-', linewidth=2)

    plt.axhline(0.0, linestyle='--', linewidth=1)

    if parser.threshold is not None:
        plt.axhline(parser.threshold, linestyle='--', linewidth=1)

    if parser.xlim is not None:
        plt.xlim(parser.xlim)

    if parser.ylim is not None:
        plt.ylim(parser.ylim)

    m_label = parser.m_label()
    plt.ylabel("dm$_{:s}$/dt (1/ns)".format(m_label))
    plt.xlabel("t (ns)")

    if parser.savepng:
        fname = "dm{:s}_dt.png".format(m_label)
        plt.savefig(fname, dpi=200, bbox_inches='tight')

    if parser.showfig:
        plt.show()
